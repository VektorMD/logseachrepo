package LogSearch;

import java.util.ArrayList;
import java.util.List;

public class LogFileReaderInfo {
    private final String path;
    private final int amountOfLines;
    private int currentPageNumber;
    private List<String> currentPageText;
    private final int PAGECOUNT;

    public LogFileReaderInfo(String path, int amountOfLines, int pageCount) {
        this.path = path;
        this.amountOfLines = amountOfLines;
        currentPageText = new ArrayList<>();
        currentPageNumber = 1;
        PAGECOUNT = pageCount;
    }

    String getPath() {
        return path;
    }

    int getAmountOfLines() {
        return amountOfLines;
    }

    public int getCurrentPageNumber() {
        return currentPageNumber;
    }

    public void setCurrentPageNumber(int currentPageNumber) {
        this.currentPageNumber = currentPageNumber;
    }

    public void setCurrentPageText(List<String> pageText) {
        currentPageText = pageText;
    }

    public List<String> getCurrentPageText() {
        return currentPageText;
    }

    public int getPageCount() {
        return PAGECOUNT;
    }
}
