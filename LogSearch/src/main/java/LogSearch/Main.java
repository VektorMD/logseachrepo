package LogSearch;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;


public class Main extends Application {
    private SearchToolController controller;

    @Override
    public void start(Stage primaryStage) throws Exception{
//        controller = new SearchToolController();
        FXMLLoader fxLoader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/searchtool.fxml"));
        Parent root = fxLoader.load();

        controller = fxLoader.getController();
        controller.setViewerTabClosingPolicy(TabPane.TabClosingPolicy.SELECTED_TAB);
        controller.initNavigationButtonsImages();

        primaryStage.setTitle("LogSearch");
//        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.setMinHeight(100);
        primaryStage.setMinWidth(200);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
