package LogSearch;

import LogSearch.Tasks.FirstPageLoadingTask;
import LogSearch.Tasks.PageLoadingTask;
import LogSearch.Tasks.PartialLogSearchTask;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import java.io.File;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class SearchToolController {
    @FXML
    private TreeView treeView;
    @FXML
    private TextField textFieldPath;
    @FXML
    private TextField textFieldQuery;
    @FXML
    private TextField textFieldExtension;
    @FXML
    private TextField textFieldCurrentPage;
    @FXML
    private ProgressIndicator searchProgressIndicator;
    @FXML
    private TabPane fileViewer;
    @FXML
    private Label labelMaxPageNumber;
    @FXML
    private Label labelLoadingFile;
    @FXML
    private Button previousPageButton;
    @FXML
    private Button nextPageButton;
    @FXML
    private Button searchButton;
    @FXML
    private ImageView imageViewFileLoading;

    private List<String> lastSearchResults;
    private List<LogFileReaderInfo> openedFileList;
    private String lastSearchLocation;
    private String lastQuery;
    private String lastExtension;
    private final int MAXLINESONPAGE = 500;
    private List<String> processedFileList;
    private PartialLogSearchTask searchTask;

    public SearchToolController() {
        lastSearchResults = new ArrayList<>();
        openedFileList = new ArrayList<>();
        processedFileList = new ArrayList<>();
    }

    public void setViewerTabClosingPolicy(TabPane.TabClosingPolicy policy) {
        fileViewer.setTabClosingPolicy(policy);
    }

    public void initNavigationButtonsImages() {
        previousPageButton.setGraphic(new ImageView(new Image("/images/icons8-left-24.png")));
        nextPageButton.setGraphic(new ImageView(new Image("/images/icons8-right-24.png")));
        imageViewFileLoading.setImage(new Image("/images/loading.gif"));
        fileViewer.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null) {
                textFieldCurrentPage.setText("");
                textFieldCurrentPage.setDisable(true);
                labelMaxPageNumber.setText("/ 0");
                previousPageButton.setDisable(true);
                nextPageButton.setDisable(true);
                return;
            }
            String currentFileName = newValue.getText();
            for (LogFileReaderInfo readerInfo: openedFileList) {
                if (readerInfo.getPath().endsWith(currentFileName)) {
                    textFieldCurrentPage.setText(String.valueOf(readerInfo.getCurrentPageNumber()));
                    labelMaxPageNumber.setText("/ " + readerInfo.getPageCount());
                    if (readerInfo.getCurrentPageNumber() == 1) {
                        previousPageButton.setDisable(true);
                    } else {
                        previousPageButton.setDisable(false);
                    }
                    if (readerInfo.getCurrentPageNumber() == readerInfo.getPageCount()) {
                        nextPageButton.setDisable(true);
                    } else {
                        nextPageButton.setDisable(false);
                    }
                }
            }
        });
        textFieldCurrentPage.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                String currentFileName = fileViewer.getSelectionModel().getSelectedItem().getText();
                for (LogFileReaderInfo readerInfo: openedFileList) {
                    if (readerInfo.getPath().endsWith(currentFileName)) {
                        textFieldCurrentPage.setText(String.valueOf(readerInfo.getCurrentPageNumber()));
                        return;
                    }
                }
            }
        });
        textFieldCurrentPage.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                textFieldCurrentPage.setText(newValue.replaceAll("[\\D]", ""));
            }
        });
    }

    public void onPreviousPageButtonClicked() {
        int pageNumber = Integer.valueOf(textFieldCurrentPage.getText());
        pageNumber--;
        loadPage(pageNumber);
    }

    public void onNextPageButtonClicked() {
        int pageNumber = Integer.valueOf(textFieldCurrentPage.getText());
        pageNumber++;
        loadPage(pageNumber);
    }

    public void textFieldCurrentPageOnAction() {
        loadPage(Integer.valueOf(textFieldCurrentPage.getText()));
    }

    public void textFieldQueryOnAction() {
        onSearchButtonClicked();
    }

    private void loadPage (int pageNumber) {
        if (pageNumber < 1) {
            return;
        }
        Tab currentTab = fileViewer.getSelectionModel().getSelectedItem();
        String fileName = currentTab.getText();

        for (LogFileReaderInfo readerInfo: openedFileList) {
            if (readerInfo.getPath().endsWith(fileName)) {
                if (pageNumber > readerInfo.getPageCount()) {
                    return;
                }
                textFieldCurrentPage.setDisable(true);
                previousPageButton.setDisable(true);
                nextPageButton.setDisable(true);
                PageLoadingTask task = new PageLoadingTask(readerInfo.getPath(), MAXLINESONPAGE, pageNumber);
                task.setOnSucceeded(event1 -> {
                    List<String> page = task.getValue();
                    readerInfo.setCurrentPageText(page);
                    readerInfo.setCurrentPageNumber(pageNumber);
                    TextArea text = (TextArea) currentTab.getContent();
                    text.clear();
                    for (String s: readerInfo.getCurrentPageText()) {
                        text.appendText(s + "\n");
                    }
                    text.positionCaret(0);
                    textFieldCurrentPage.setDisable(false);
                    labelMaxPageNumber.setDisable(false);
                    if (currentTab == fileViewer.getSelectionModel().getSelectedItem()) {
                        textFieldCurrentPage.setText(String.valueOf(pageNumber));
                        labelMaxPageNumber.setText("/ " + readerInfo.getPageCount());
                    }
                    boolean previousPageButtonDisableProperty = false;
                    boolean nextPageButtonDisableProperty = false;
                    if (pageNumber == 1) {
                        previousPageButtonDisableProperty = true;
                    }
                    if (pageNumber == readerInfo.getPageCount()) {
                        nextPageButtonDisableProperty = true;
                    }
                    previousPageButton.setDisable(previousPageButtonDisableProperty);
                    nextPageButton.setDisable(nextPageButtonDisableProperty);
                });
                Thread taskThread = new Thread(task);
                taskThread.start();
                return;
            }
        }
    }

    private class OpenFileOnDoubleClick implements EventHandler<MouseEvent> {

        @Override
        public void handle(MouseEvent event) {
            if (event.getClickCount() == 2) {
                TreeItem<String> item = (TreeItem<String>) treeView.getSelectionModel().getSelectedItem();
                if (item == null) {
                    return;
                }
                if (item.getChildren().size() > 0) {
                    // Это папка
                    return;
                }
                String fileName = item.getValue();
                if (processedFileList.contains(fileName)) {
                    return;
                }
                for (Tab tab: fileViewer.getTabs()) {
                    if (tab.getText().equals(fileName)) {
                        SingleSelectionModel<Tab> selectionModel = fileViewer.getSelectionModel();
                        selectionModel.select(tab);
                        return;
                    }
                }
                for (String f: lastSearchResults) {
                    String[] pathParts = f.split("\\\\");
                    if (pathParts[pathParts.length - 1].equals(fileName)) {
                        processedFileList.add(fileName);
                        String workingOn = "";
                        for (int i = 0; i < processedFileList.size(); i++) {
                            if (i == processedFileList.size()) {
                                workingOn = workingOn + processedFileList.get(i);
                            } else {
                                workingOn = workingOn + processedFileList.get(i) + ", ";
                            }
                        }
                        labelLoadingFile.setText(workingOn);
                        FirstPageLoadingTask task = new FirstPageLoadingTask(f, MAXLINESONPAGE);
                        labelLoadingFile.setVisible(true);
                        imageViewFileLoading.setVisible(true);

                        task.setOnFailed(event1 -> {
                            labelLoadingFile.setVisible(false);
                            imageViewFileLoading.setVisible(false);
                        });

                        task.setOnSucceeded(event1 -> {
                            LogFileReaderInfo readerInfo = task.getValue();
                            Tab tab = new Tab(fileName);
                            tab.setOnClosed(event2 -> {
                                for (LogFileReaderInfo info: openedFileList) {
                                    if (info.getPath().endsWith(fileName)) {
                                        openedFileList.remove(info);
                                        break;
                                    }
                                }
                            });
                            TextArea text = new TextArea();
                            text.setWrapText(true);
                            text.setMaxHeight(Double.MAX_VALUE);
                            for (String s: readerInfo.getCurrentPageText()) {
                                text.appendText(s + "\n");
                            }
                            text.positionCaret(0);
                            tab.setContent(text);
                            fileViewer.getTabs().add(tab);
                            SingleSelectionModel<Tab> selectionModel = fileViewer.getSelectionModel();
                            selectionModel.select(tab);
                            labelMaxPageNumber.setText("/ " + readerInfo.getPageCount());
                            textFieldCurrentPage.setText("1");
                            processedFileList.remove(fileName);

                            String workingOnF = "";
                            for (int i = 0; i < processedFileList.size(); i++) {
                                if (i == processedFileList.size()) {
                                    workingOnF = workingOnF + processedFileList.get(i);
                                } else {
                                    workingOnF = workingOnF + processedFileList.get(i) + ", ";
                                }
                            }
                            labelLoadingFile.setText(workingOnF);
                            openedFileList.add(readerInfo);
                            if (processedFileList.size() == 0) {
                                textFieldCurrentPage.setDisable(false);
                                previousPageButton.setDisable(true);
                                if (readerInfo.getPageCount() == readerInfo.getCurrentPageNumber()) {
                                    nextPageButton.setDisable(true);
                                } else {
                                    nextPageButton.setDisable(false);
                                }
                                labelLoadingFile.setVisible(false);
                                imageViewFileLoading.setVisible(false);
                            }
                        });
                        Thread taskThread = new Thread(task);
                        taskThread.start();
                        return;
                    }
                }

            }
        }
    }

    private void setSearchFormState(boolean isSearching) {
        textFieldPath.setDisable(isSearching);
        textFieldQuery.setDisable(isSearching);
        textFieldExtension.setDisable(isSearching);
        if (isSearching) {
            searchButton.setText("Cancel");
        } else {
            searchButton.setText("Search");
        }
    }

    public void onSearchButtonClicked() {
        if (searchButton.getText().equals("Cancel")) {
            searchTask.cancel();
        } else {
            if (searchButton.getText().equals("Search")) {
                search();
            }
        }
    }

    private void search() {
        if (textFieldQuery.getText().length() == 0
                || textFieldExtension.getText().length() == 0
                || textFieldPath.getText().length() == 0) {
            return;
        }

        File testPath = new File(textFieldPath.getText());
        if (!testPath.isDirectory()) {
            return;
        }

        treeView.setRoot(null);
        treeView.setOnMouseClicked(null);
        setSearchFormState(true);

        lastSearchLocation = textFieldPath.getText();
        lastQuery = textFieldQuery.getText();
        lastExtension = textFieldExtension.getText();

        searchTask = new PartialLogSearchTask(lastSearchLocation, lastQuery, lastExtension);
        String[] pathParts = lastSearchLocation.split("\\\\");
        String rootDirName = pathParts[pathParts.length - 1];

        searchProgressIndicator.progressProperty().unbind();
        searchProgressIndicator.progressProperty().bind(searchTask.progressProperty());
        searchTask.getPartialResults().addListener((ListChangeListener) c -> {
            while (c.next()) {
                if (c.wasAdded()) {
                    List<File> subList = c.getAddedSubList();
                    for (File f: subList) {
                        addFileToTreeView(f.getAbsolutePath());
                    }
                }
            }
        });

        searchTask.setOnCancelled(event -> setSearchFormState(false));

        searchTask.setOnSucceeded(event -> setSearchFormState(false));

        Thread taskThread = new Thread(searchTask);
        taskThread.start();
    }

    private void addFileToTreeView(String absolutePath) {
        String[] rootDirPathElements = lastSearchLocation.split("\\\\");
        String rootDirName = rootDirPathElements[rootDirPathElements.length - 1];

        TreeItem<String> rootItem;
        if (treeView.getRoot() != null) {
            rootItem = treeView.getRoot();
        } else {
            rootItem = new TreeItem<>(rootDirName, new ImageView(new Image(getClass().getResourceAsStream("/images/folder-20px.png"))));
            treeView.setRoot(rootItem);
        }

        lastSearchResults.add(absolutePath);
        String[] pParts = absolutePath.split("\\\\");
        boolean flagFoundRoot = false;
        TreeItem tmp = null;
        for (int i = 0; i < pParts.length; i++) {
            if (pParts[i].equals(rootDirName)) {
                flagFoundRoot = true;
                tmp = rootItem;
                continue;
            }
            if (flagFoundRoot) {
                // Последняя часть пути -- имя файла
                if (i == pParts.length - 1) {
                    TreeItem item = new TreeItem(pParts[i], new ImageView(new Image(getClass().getResourceAsStream("/images/document-20px.png"))));
                    tmp.getChildren().add(item);
                    break;
                }
                // Проверяем, есть ли уже узел папки в дереве
                ObservableList<TreeItem<String>> nodes = tmp.getChildren();
                boolean flagHasNode = false;
                for (TreeItem<String> treeItem : nodes) {
                    if (treeItem.getValue().equals(pParts[i])) {
                        tmp = treeItem;
                        flagHasNode = true;
                        break;
                    }
                }
                if (!flagHasNode) {
                    TreeItem item = new TreeItem(pParts[i], new ImageView(new Image(getClass().getResourceAsStream("/images/folder-20px.png"))));
                    tmp.getChildren().add(item);
                    tmp = item;
                }
            }
        }

        treeView.setOnMouseClicked(new OpenFileOnDoubleClick());
    }
}
