package LogSearch.Tasks;

import LogSearch.LogFileReaderInfo;
import javafx.concurrent.Task;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FirstPageLoadingTask extends Task<LogFileReaderInfo> {
    final String filePath;
    final int maxLinesOnPage;

    public FirstPageLoadingTask(String filePath, int maxLinesOnPage) {
        this.filePath = filePath;
        this.maxLinesOnPage = maxLinesOnPage;
    }

    @Override
    protected LogFileReaderInfo call() throws Exception {
        int amountOfLines = 0;
        List<String> firstPage = new ArrayList<>();

        try (Scanner in = new Scanner(new FileReader(filePath))){
            while (in.hasNextLine()) {
                if (amountOfLines < maxLinesOnPage) {
                    firstPage.add(in.nextLine());
                } else {
                    in.nextLine();
                }
                amountOfLines++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        int maxPageNumber = (int) Math.ceil(((double) amountOfLines) / maxLinesOnPage);
        LogFileReaderInfo readerInfo = new LogFileReaderInfo(filePath, amountOfLines, maxPageNumber);
        readerInfo.setCurrentPageNumber(1);
        readerInfo.setCurrentPageText(firstPage);
        return readerInfo;
    }
}
