package LogSearch.Tasks;

import javafx.concurrent.Task;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PageLoadingTask extends Task<List<String>> {
    final String filePath;
    final int maxLinesOnPage;
    final int pageNumber;

    public PageLoadingTask(String filePath, int maxLinesOnPage, int pageNumber) {
        this.filePath = filePath;
        this.maxLinesOnPage = maxLinesOnPage;
        this.pageNumber = pageNumber;
    }

    @Override
    protected List<String> call() {
        int lineNumber = 0;
        List<String> page = new ArrayList<>();
        int firstLineToSave = ((pageNumber - 1) * 500 + 1);
        int lastLineToSave = (pageNumber * 500);
        try (Scanner in = new Scanner(new FileReader(filePath))){
            while (in.hasNextLine() && (lineNumber + 1) <= lastLineToSave) {
                lineNumber++;
                if (lineNumber >= firstLineToSave) {
                    page.add(in.nextLine());
                } else {
                    in.nextLine();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return page;
    }
}
