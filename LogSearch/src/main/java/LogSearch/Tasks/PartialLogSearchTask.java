package LogSearch.Tasks;

import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PartialLogSearchTask extends Task<ObservableList<File>> {
    private String rootDir;
    private String query;
    private String extension;


    public PartialLogSearchTask(String rootDirectory, String query, String extension) {
        rootDir = rootDirectory;
        this.query = query;
        this.extension = extension;
    }


    private ReadOnlyObjectWrapper<ObservableList> partialResult = new ReadOnlyObjectWrapper<>(this, "partialResults", FXCollections.observableArrayList(new ArrayList()));
    public final ObservableList getPartialResults() {
        return partialResult.get();
    }

    public final ReadOnlyProperty partialResultsProperty() {
        return partialResult.getReadOnlyProperty();
    }


    @Override
    protected ObservableList<File> call() {
        List<File> files = findLogFiles(new File(rootDir));
        int filesToProcess = files.size();
        String queryLow = query.toLowerCase();
        for (File f: findLogFiles(new File(rootDir))) {
            if (isCancelled()) {
                break;
            }
            boolean queryFound = false;
            try (Scanner in = new Scanner(new FileReader(f))){
                while (in.hasNextLine() && !queryFound && !isCancelled()) {
                    queryFound = in.nextLine().toLowerCase().contains(queryLow);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (queryFound) {
                Platform.runLater(() -> {
                    partialResult.get().add(f);
                });
            }
            filesToProcess--;
            updateProgress(files.size() - filesToProcess, files.size());
        }
        updateProgress(files.size(), files.size());
        return partialResult.get();
    }

    private List<File> findLogFiles(File dir) {
        List<File> logList = new ArrayList<>();
        File[] list = dir.listFiles();
        if (list == null) {
            return null;
        }

        for (File f: dir.listFiles()) {
            if (f.isDirectory()) {
                for (File tmp : findLogFiles(f)) {
                    logList.add(tmp);
                }
            } else {
                if (f.getName().endsWith(extension)) {
                    logList.add(f);
                }
            }
        }
        return logList;
    }
}
